//
//  main.m
//  League of RetroHeroes Companion App
//
//  Created by juanjo on 17/12/17.
//  Copyright © 2017 caballerogo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
