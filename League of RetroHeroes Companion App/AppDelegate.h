//
//  AppDelegate.h
//  League of RetroHeroes Companion App
//
//  Created by juanjo on 17/12/17.
//  Copyright © 2017 caballerogo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

