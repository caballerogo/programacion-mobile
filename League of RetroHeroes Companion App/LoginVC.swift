//
//  LoginVC.swift
//  League of RetroHeroes Companion App
//
//  Created by juanjo on 17/12/17.
//  Copyright © 2017 caballerogo. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var userTF: UITextField!
    @IBOutlet weak var passTF: UITextField!
    @IBOutlet weak var persistent: UISwitch!
    @IBOutlet weak var ErrorLabel: UILabel!
    
    struct pData{
        static let nick_ = ""
        static let persistent_ = "no"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        ErrorLabel.text = ""
        
        let defaults = UserDefaults.standard
        
        if(defaults.string(forKey: pData.persistent_) == "yes"){
            persistent.isOn = true
            userTF.text = defaults.string(forKey: pData.nick_)
        }else{
            persistent.isOn = false
            userTF.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func LogginAttempt(_ sender: Any) {
        
        if(userTF.text == "caballerogo"){
            if(passTF.text == "esat"){
                if(persistent.isOn){
                    let defaults = UserDefaults.standard
                    defaults.set("yes", forKey: pData.persistent_)
                    defaults.set(userTF.text, forKey: pData.nick_)
                }else{
                    let defaults = UserDefaults.standard
                    defaults.set("no", forKey: pData.persistent_)
                    defaults.set("", forKey: pData.nick_)
                }
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Success") as! SuccessVC
                self.present(vc, animated: true, completion: nil)
            }else{
                ErrorLabel.text = "Wrong Password"
            }
        }else{
            ErrorLabel.text = "Wrong Username"
        }
        
    }
    @IBAction func SetPersistentSettings(_ sender: Any) {
        if(persistent.isOn == false){
            let defaults = UserDefaults.standard
            defaults.set("no", forKey: pData.persistent_)
        }
    }
    

}
